import  {initialStates} from "../../redux/store/initialState";
import ACTIONS from "../../redux/actions";
import {RequestStatus,STATUS} from "../requestStatus";

const twitterReducer = (state = initialStates, action: any) => {
    const twitterRequestState = RequestStatus(ACTIONS.API.TWITTER.GET_USER_TIMELINE);
    switch (action.type) {
        case  twitterRequestState.FETCH:
        case twitterRequestState.REQUESTED:
            return {
                ...state,
                status: STATUS.FETCHING
            };
        case twitterRequestState.SUCCEEDED:
            console.log('succeeded,', action);
            return {
                ...state,
                tweets: action.tweets,
                status: STATUS.DONE
            };
        case twitterRequestState.FAILED:
            return {
                ...state,
                status: STATUS.DONE
            };
        default:
            return {
                ...state
            };
    }
};

export {twitterReducer}