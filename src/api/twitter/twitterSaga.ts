import {put, takeLatest} from 'redux-saga/effects'
import {RequestStatus} from "../requestStatus";
import ACTIONS from "../../redux/actions";
import {HttpRequest, networkManager} from "../../modules/network/networkInterface";
import API_URL from "../url";

const twitterRequestState = RequestStatus(ACTIONS.API.TWITTER.GET_USER_TIMELINE);


/**
 * ACTION CREATORS
 */
const twitterApiFetch = () => {
    return {type: twitterRequestState.FETCH}
};
const twitterApiRequested = () => {
    return {type: twitterRequestState.REQUESTED}
};
const twitterApiRequestedSucc = (tweets: Array<object>) => {
    console.log('twitter requested succ,', tweets);
    return {
        type: twitterRequestState.SUCCEEDED,
        tweets
    }
};

const twitterRequestedErr = (error: string) => {
    return {
        type: twitterRequestState.FAILED,
        error
    }
};


/**
 * @description tweet service worker function
 * @param action
 */
function* getTweets(action: object) {
    try {
        yield put(twitterApiRequested());
        const {data} =
            yield networkManager.sendRequest(
                new HttpRequest(
                    API_URL.TWITTER.BASE_URL + API_URL.TWITTER.APIS.USER_TIMELINE.PATH, {
                        headers: API_URL.TWITTER.HEADERS
                    }));
        yield put(twitterApiRequestedSucc(data));
    } catch (e) {
        yield put(twitterRequestedErr(e));
    }
}

/**
 * @description twitter function watcher
 */
function* twitterSaga() {
    yield takeLatest(twitterRequestState.FETCH, getTweets);
}


export {twitterSaga, twitterApiFetch};