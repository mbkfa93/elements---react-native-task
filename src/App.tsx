/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Provider} from "react-redux";

import {store, sagaMiddleware} from "./redux/store/store";
import rootSaga from "./redux/saga/rootSagaHandler";
import AppContainer from './navigation/navigation';

sagaMiddleware.run(rootSaga);


export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <AppContainer/>
            </Provider>
        );
    }
}