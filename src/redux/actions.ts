const ACTIONS = {
    USER: {
        REGISTER: {
            REGISTER_REQUEST: "REGISTER_REQUEST",
            REGISTER_REQUEST_LOADING:"REGISTER_REQUEST_LOADING",
            REGISTER_SUCCESS: "REGISTER_SUCCESS",
            REGISTER_FAIL: "REGISTER_FAIL"
        },
        LOGIN: {
            LOGIN_REQUEST: "LOGIN_REQUEST",
            LOGIN_REQUEST_LOADING: "LOGIN_REQUEST_LOADING",
            LOGIN_SUCCESS: "LOGIN_SUCCESS",
            LOGIN_FAIL: "LOGIN_FAILED"
        },
    },
    API: {
        TWITTER: {
            GET_USER_TIMELINE: 'API/TWITTER/GET_USER_TIMELINE'
        }
    },
    MODULE:{
        LOCAL_STORAGE:{
            INIT:{
                INIT_REQUESTED:"LOCAL_STORAGE_INIT_REQUESTED",
                INIT_SUCCESS:"LOCAL_STORAGE_INIT_SUCCESS",
                INIT_ERROR:"LOCAL_STORAGE_INIT_ERROR"
            },
            ADD:{
                ADD_REQUESTED:"LOCAL_STORAGE_ADD_REQUESTED",
                ADD_SUCCESS:"LOCAL_STORAGE_ADD_SUCCESS",
                ADD_ERROR:"LOCAL_STORAGE_ADD_ERROR"
            },
            GET:{
                GET_REQUESTED:"LOCAL_STORAGE_GET_REQUESTED",
                GET_SUCCESS:"LOCAL_STORAGE_GET_SUCCESS",
                GET_ERROR:"LOCAL_STORAGE_GET_ERROR"
            }

        }
    }
}

export default ACTIONS;