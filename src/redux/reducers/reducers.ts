import {combineReducers} from "redux";
import {twitterReducer} from "../../api/twitter/twitterReducer";
import {loginReducer} from "../../components/login/LoginReducer";
import {RegistrationReducer} from "../../components/registration/RegistrationReducer";
import {localStorageReducer} from "../../modules/offline_storage/localStorageReducer";


/**
 * @description check https://github.com/reduxjs/redux/issues/2709#issuecomment-357328709
 * for explaining the usage of 'as any'
 */
const reducers = combineReducers({
    twitterReducer,
    loginReducer,
    RegistrationReducer,
    localStorageReducer
} as any);


// const reducers =
export {reducers};