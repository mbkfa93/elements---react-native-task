/**
 * @file networkInterfaces: Only this file should be included for any feature that requires networking access
 */
import {NetworkManager, HttpBaseInstance, HttpRequest, Axios} from "./NetworkManager";

const networkManager: NetworkManager = new NetworkManager({
    instance: Axios.create(),
    managerType: NetworkManager.manager.AXIOS,
    instanceOptions: new HttpBaseInstance({
    })
});

export {networkManager, HttpRequest, HttpBaseInstance};