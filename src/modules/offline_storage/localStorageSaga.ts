/**
 * NOTE
 * This SQLite storage was meant to be used for storing users offline in the app.
 * but it took some more time than expected, and for that, it was replaced with  AsyncStorage
 * AsyncStorage is a much faster solution, and enough for the purpose of that task
 */



import {put, takeLatest, call} from 'redux-saga/effects'

import ACTIONS from "../../redux/actions";
import {yieldCombinedSagas} from "../../redux/saga/sagaHelper";

/**
 * ACTION CREATORS
 */

const initDatabase_actionCreator = () => {
    return {
        type: ACTIONS.MODULE.LOCAL_STORAGE.INIT.INIT_REQUESTED
    }
}

const initDatabaseSuccess_actionCreator = () => {
    return {
        type: ACTIONS.MODULE.LOCAL_STORAGE.INIT.INIT_SUCCESS
    }
}
const initDatabaseError_actionCreator = (error: any) => {
    return {
        type: ACTIONS.MODULE.LOCAL_STORAGE.INIT.INIT_ERROR,
        error
    }
}

const getUser_actionCreator = (credentials: object) => {
    return {
        type: ACTIONS.MODULE.LOCAL_STORAGE.GET.GET_REQUESTED,
        payload: {
            user: {
                credentials
            }
        }
    }
};
const getUserSuccess_actionCreator = (data: any) => {
    return {
        type: ACTIONS.MODULE.LOCAL_STORAGE.GET.GET_SUCCESS
    }
};


const getUserError_actionCreator = (data: any) => {
    return {
        type: ACTIONS.MODULE.LOCAL_STORAGE.GET.GET_ERROR
    }
}


const addUser_actionCreator = (credentials: object) => {
    return {
        type: ACTIONS.MODULE.LOCAL_STORAGE.ADD.ADD_REQUESTED,
        payload: {
            user: {
                credentials
            }
        }
    }
};
const addtUserSuccess_actionCreator = (data: any) => {
    return {
        type: ACTIONS.MODULE.LOCAL_STORAGE.ADD.ADD_SUCCESS
    }
};


const addUserError_actionCreator = (data: any) => {
    return {
        type: ACTIONS.MODULE.LOCAL_STORAGE.ADD.ADD_ERROR
    }
}


/**
 * Worker functions
 */

function* closeDb(db: any) {
    yield new Promise((resolve => {

        console.log("Closing DB");
        db.close()
            .then((status: any) => {
                console.log("Database CLOSED");
                resolve(true);
            })
            .catch((error: any) => {
                resolve(error);
            });
    }))
    yield console.log("DB Closed");
}

function initDb() {
    return  new Promise((resolve) => {
        console.log("Plugin integrity check ...");

        console.log("Integrity check passed ...");
        console.log("Opening database ...");
        let db: any;
    });
}


function* addUser(data: any) {
    yield  true;
    // yield put(loginRequested());
    try {
        // const result = yield  login(action.payload.user.credentials.login);
        // console.log("Succ from loginRequest saga",result);
        yield put(addtUserSuccess_actionCreator(data));
    } catch (e) {
        yield put(addUserError_actionCreator(e));
    }

}

function* getUser(data: any) {
    console.log(data.payload.user.credentials);


    try {

        const user = yield new Promise((resolve) => {
            const db = initDb();
            initDb().then((db:any) => {
                db.transaction((tx: any) => {
                    tx.executeSql('SELECT username,password FROM table_user ', []).then(([tx,results]:any) => {
                        console.log("Query completed");
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            console.log('results', row)
                        }

                        resolve(results.rows);
                    })
                }).then((result: any) => {
                    closeDb(db)
                }).catch((err: any) => {
                    console.log(err);
                });
            })
        });
// yield put(loginRequested());
        put(getUserSuccess_actionCreator(data));
    } catch (e) {
        console.error(e);
        put(getUserError_actionCreator(e));
    }
}


/**
 * @description  function watcher
 */
function* localStorageSaga() {
    yield takeLatest(ACTIONS.MODULE.LOCAL_STORAGE.INIT.INIT_REQUESTED, initDb);
    yield takeLatest(ACTIONS.MODULE.LOCAL_STORAGE.ADD.ADD_REQUESTED, addUser);
    yield takeLatest(ACTIONS.MODULE.LOCAL_STORAGE.GET.GET_REQUESTED, getUser);
}


export {localStorageSaga, initDatabase_actionCreator, addUser_actionCreator, getUser_actionCreator};
