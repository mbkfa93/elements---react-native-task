import {initialStates} from "../../redux/store/initialState";
import ACTIONS from "../../redux/actions";


const localStorageReducer = (state = initialStates, action: any) => {

    switch (action.type) {
        case  ACTIONS.MODULE.LOCAL_STORAGE.INIT.INIT_REQUESTED:
        case  ACTIONS.MODULE.LOCAL_STORAGE.INIT.INIT_SUCCESS:
        case  ACTIONS.MODULE.LOCAL_STORAGE.INIT.INIT_ERROR:
            return {
                ...state,
            };

        case  ACTIONS.MODULE.LOCAL_STORAGE.GET.GET_REQUESTED:
        case  ACTIONS.MODULE.LOCAL_STORAGE.GET.GET_ERROR:
            return {...state};

        case  ACTIONS.MODULE.LOCAL_STORAGE.GET.GET_SUCCESS:
            return {
                ...state,
                userData: action.payload.userData
            };

        case  ACTIONS.MODULE.LOCAL_STORAGE.ADD.ADD_REQUESTED:
            return {...state};

        case  ACTIONS.MODULE.LOCAL_STORAGE.ADD.ADD_SUCCESS:
            return {
                ...state,
                userData: action.payload.userData
            };
        case  ACTIONS.MODULE.LOCAL_STORAGE.ADD.ADD_ERROR:
            return {...state};

        default:
            return {
                ...state
            };
    }
};

export {localStorageReducer}