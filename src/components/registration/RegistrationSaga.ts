import {put, takeLatest} from 'redux-saga/effects'

import ACTIONS from "../../redux/actions";
import AsyncStorage from "@react-native-community/async-storage";

/**
 * ACTION CREATORS
 */
const RegisterNewUser = (credentials: object) => {
    return {
        type: ACTIONS.USER.REGISTER.REGISTER_REQUEST,
        payload: {
            user: {
                credentials
            }
        }
    }
};
const registrationRequested = () => {
    return {type: ACTIONS.USER.REGISTER.REGISTER_REQUEST_LOADING}
};

const registrationSuccess = () => {
    return {
        type: ACTIONS.USER.REGISTER.REGISTER_SUCCESS,
    }
};

const registrationError = (error: string) => {
    return {
        type: ACTIONS.USER.REGISTER.REGISTER_FAIL,
        error
    }
};



/**
 * @description Registrationservice worker function
 * @param action
 */
function* registerRequest(action: any) {
    yield put(registrationRequested());
    try {
        yield AsyncStorage.setItem('user', JSON.stringify(action.payload.user.credentials.register));
        console.log("Registered successfully", action.payload.user.credentials.register);
        yield put(registrationSuccess());
    } catch (e) {
        yield put(registrationError(e));
    }
}


/**
 * @description function watcher
 */
function* RegistrationSaga() {
    yield takeLatest(ACTIONS.USER.REGISTER.REGISTER_REQUEST, registerRequest);
}

export {RegistrationSaga, RegisterNewUser};