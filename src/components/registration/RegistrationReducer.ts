import {initialStates} from "../../redux/store/initialState";
import ACTIONS from "../../redux/actions";


const RegistrationReducer = (state = initialStates, action: any) => {
    switch (action.type) {
        case  ACTIONS.USER.REGISTER.REGISTER_REQUEST:
            return {
                ...state,
                user: {
                    register: action.payload.user.credentials.register
                },
                status: ACTIONS.USER.REGISTER.REGISTER_REQUEST_LOADING
            };

        case  ACTIONS.USER.REGISTER.REGISTER_REQUEST_LOADING:
            return {
                ...state,
                isLoading: true
            };
        case  ACTIONS.USER.REGISTER.REGISTER_SUCCESS:
            console.log('Registration success');
            return {
                ...state,
                isLoading: false,
                status: ACTIONS.USER.REGISTER.REGISTER_SUCCESS
            };
        case  ACTIONS.USER.LOGIN.LOGIN_FAIL:
            console.log('Registration failed');
            return {
                ...state,
                isLoading: false,
                status: ACTIONS.USER.REGISTER.REGISTER_FAIL
            };
        default:
            return {
                ...state
            };
    }
};

export {RegistrationReducer}