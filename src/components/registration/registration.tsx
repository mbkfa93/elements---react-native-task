import React, {Component} from 'react';
import {StyleSheet, Text, AppRegistry, Image, ScrollView} from 'react-native';

import {RegisterNewUser} from "./RegistrationSaga";
import {connect} from "react-redux";
import ACTIONS from "../../redux/actions";
import {Button, Container, Icon, Input, InputGroup, Item} from "native-base";

class RegistrationScreen extends Component<any, any> {
    userCredentials = {
        register: {
            userName: '',
            password: "",
            firstName: "",
            lastName: ""

        }
    };
    submit = () => {
        console.log('submit clicked', this.userCredentials);
        this.props.registerUser(this.userCredentials)
    };

    componentDidUpdate(prevProps: Readonly<any>, prevState: Readonly<any>, snapshot?: any): void {
        if (this.props.registrationStatus == ACTIONS.USER.REGISTER.REGISTER_SUCCESS) {
            this.props.navigation.navigate('Home');
        }
    }


    render() {
        return (
            <ScrollView style={{flex: 1}}
                        scrollEnabled={true}>
                <Container style={styles.container}>
                    <Image source={require('../../assets/img/logo.jpg')}/>

                    <Item underline>
                        <InputGroup>
                            <Icon name="user" type='FontAwesome' style={{color: '#384850'}}/>
                            <Input placeholder='Username' onChangeText={(_userName) => {
                                this.userCredentials.register.userName = _userName
                            }}/>
                        </InputGroup>
                    </Item>

                    <Item underline>
                        <InputGroup>
                            <Icon name="lock" type='FontAwesome' style={{color: '#384850'}}/>
                            <Input secureTextEntry blurOnSubmit placeholder='Password' onChangeText={(_password) => {
                                this.userCredentials.register.password = _password
                            }}/>
                        </InputGroup>
                    </Item>


                    <Item underline>
                        <InputGroup>
                            {/*<Icon name="user" type='FontAwesome' style={{color: '#384850'}}/>*/}
                            <Input placeholder='First Name' onChangeText={(_firstName) => {
                                this.userCredentials.register.firstName = _firstName
                            }}/>
                        </InputGroup>
                    </Item>

                    <Item underline>
                        <InputGroup>
                            {/*<Icon name="lock" type='FontAwesome' style={{color: '#384850'}}/>*/}
                            <Input placeholder='Last Name' onChangeText={(_lastName) => {
                                this.userCredentials.register.lastName = _lastName
                            }}/>
                        </InputGroup>
                    </Item>

                    <Button style={styles.button} block dark rounded
                            onPress={() => this.submit()}>
                        <Text style={styles.buttonText}>Register</Text>
                    </Button>
                </Container>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        margin: 20
    },
    button: {
        flex: 1,
        justifyContent: 'center',
        margin: 5,
        color: '#fa3',
        marginTop: 15
    },
    buttonText: {
        color: '#fff',

    }
});

const mapStateToProps = (state: any) => {
    return {
        registrationStatus: state.RegistrationReducer.status,
        userCredentials: state.RegistrationReducer.user
    }
};
const mapDispatchToProps = (dispatch: any) => {
    return {
        registerUser: (credentials: any) => dispatch(RegisterNewUser(credentials))
    }
};


AppRegistry.registerComponent('RegistrationScreen', () => RegistrationScreen);

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationScreen);