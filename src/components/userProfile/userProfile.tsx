import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    AppRegistry, Image
} from 'react-native';

import AsyncStorage from "@react-native-community/async-storage";
import {Container} from "native-base";


export default class UserProfileScreen extends Component<any, any> {

    async componentDidMount() {
        /**
         * TODO: replace the direct implementation for AsyncStorage with saga
         */
        const userData: any = await AsyncStorage.getItem('user');
        try {
            let {firstName, lastName, userName, password} = JSON.parse(userData);
            this.setState({
                firstName, lastName, userName, password
            })
        } catch (e) {

        }
    }


    render() {
        return (
            <Container style={styles.container}>
                <Image source={require('../../assets/img/user.png')} style={styles.profilePicture}/>
                <Text style={styles.firstLastName}>
                    {(this.state && this.state.firstName ? this.state.firstName : null) + " " + (this.state && this.state.lastName ? this.state.lastName : null)}
                </Text>

                <Text
                    style={styles.username}>{this.state && this.state.userName ? '@' + this.state.userName : null}</Text>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        margin: 20
    },
    firstLastName: {
        fontWeight: 'bold'
    },
    username: {
        textAlign: 'center',
        color: '#8b8b8b',
        marginBottom: 5,
    },
    profilePicture: {}
});

AppRegistry.registerComponent('UserProfileScreen', () => UserProfileScreen);
