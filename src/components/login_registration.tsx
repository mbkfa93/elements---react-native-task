import React, {Component} from 'react';
import {Button, Container} from "native-base";
import {StyleSheet, Text, AppRegistry, Image} from 'react-native';
import {connect} from "react-redux";

class LoginRegistrationScreen extends Component<any, any> {
    navigateToRoute = (routeName: string) => {
        this.props.navigation.navigate(routeName);
    };

    render() {
        return (
            <Container style={styles.container}>
                <Image source={require('../assets/img/logo.jpg')}/>
                <Button style={styles.button} block dark rounded
                        onPress={() => this.navigateToRoute('Login')}>
                    <Text style={styles.buttonText}>Login</Text>
                </Button>
                <Button style={styles.button} block rounded dark
                        onPress={() => this.navigateToRoute('Registration')}>
                    <Text style={styles.buttonText}>Register</Text>
                </Button>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        margin: 20
    },
    button: {
        flex: 1,
        justifyContent: 'center',
        margin: 5,
        color: '#fa3'
    },
    buttonText: {
        color: '#fff',
    }
});


const mapStateToProps = (state: any) => {
    // ...state
    return {
        // twts: state.twitterReducer.tweets
    }
};
const mapDispatchToProps = (dispatch: any) => {
    return {
        // initDb: () => dispatch(initDatabase_actionCreator())
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(LoginRegistrationScreen);
AppRegistry.registerComponent('LoginRegistrationScreen', () => LoginRegistrationScreen);

