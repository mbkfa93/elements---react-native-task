import {initialStates} from "../../redux/store/initialState";
import ACTIONS from "../../redux/actions";


const loginReducer = (state = initialStates, action: any) => {
    switch (action.type) {
        case  ACTIONS.USER.LOGIN.LOGIN_REQUEST:
            return {
                ...state,
                user: {
                    login: action.payload.user.credentials.login
                },
                status: ACTIONS.USER.LOGIN.LOGIN_REQUEST_LOADING
            };

        case  ACTIONS.USER.LOGIN.LOGIN_REQUEST_LOADING:
            return {
                ...state,
                isLoading: true
            };
        case  ACTIONS.USER.LOGIN.LOGIN_SUCCESS:
            console.log('login success');
            return {
                ...state,
                isLoading: false,
                status: ACTIONS.USER.LOGIN.LOGIN_SUCCESS
            };
        case  ACTIONS.USER.LOGIN.LOGIN_FAIL:
            console.log('login failed');
            //TODO: use the state value from this reducer to display the error
            return {
                ...state,
                isLoading: false,
                status: ACTIONS.USER.LOGIN.LOGIN_FAIL
            };
        default:
            return {
                ...state
            };
    }
};

export {loginReducer}