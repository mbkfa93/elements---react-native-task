import {put, takeLatest} from 'redux-saga/effects'

import ACTIONS from "../../redux/actions";
import AsyncStorage from "@react-native-community/async-storage";

/**
 * ACTION CREATORS
 */
const loginWithCredentials = (credentials: object) => {
    return {
        type: ACTIONS.USER.LOGIN.LOGIN_REQUEST,
        payload: {
            user: {
                credentials
            }
        }
    }
};
const loginRequested = () => {
    return {type: ACTIONS.USER.LOGIN.LOGIN_REQUEST_LOADING}
};

const loginSuccess = () => {
    return {
        type: ACTIONS.USER.LOGIN.LOGIN_SUCCESS,
    }
};

const loginError = (error: string) => {
    return {
        type: ACTIONS.USER.LOGIN.LOGIN_FAIL,
        error
    }
};


/**
 * Worker functions
 */


/**
 * @description login service worker function
 * @param action
 */
function* loginRequest(action: any) {
    yield put(loginRequested());
    try {
        const credentials = action.payload.user.credentials.login;
        const result = yield AsyncStorage.getItem('user');
        const {userName, password} = JSON.parse(result);
        if (userName == credentials.userName && password == credentials.password) {
            yield put(loginSuccess());
        } else {
            yield put(loginError("Username and password not matching"));
        }
    } catch (e) {
        yield put(loginError(e));
    }
}


/**
 * @description twitter function watcher
 */
function* LoginSaga() {
    yield takeLatest(ACTIONS.USER.LOGIN.LOGIN_REQUEST, loginRequest);
}


export {LoginSaga, loginWithCredentials};