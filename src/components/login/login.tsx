import React, {Component} from 'react';
import {StyleSheet, Text, AppRegistry, Image, ScrollView} from 'react-native';
import {connect} from "react-redux";
import {loginWithCredentials} from "./LoginSaga";
import ACTIONS from "../../redux/actions";
import {Button, Container, Icon, Input, InputGroup, Item} from "native-base";

class LoginScreen extends Component<any, any> {
    userCredentials = {
        login: {
            userName: '',
            password: ""
        }
    };

    submit = () => {
        console.log('Submit clicked,Credentials are:', this.userCredentials);
        this.props.login(this.userCredentials)
    };

    /**
     * Quick workaround for the navigation
     */
    componentDidUpdate(prevProps: Readonly<any>, prevState: Readonly<any>, snapshot?: any): void {
        if (this.props.loginStatus == ACTIONS.USER.LOGIN.LOGIN_SUCCESS) {
            this.props.navigation.navigate('Home');
        }
    }


    render() {
        return (
            <ScrollView style={{flex: 1}}
                        scrollEnabled={true}>
                <Container style={styles.container}>
                    <Image source={require('../../assets/img/logo.jpg')}/>
                    <Item underline>
                        <InputGroup>
                            <Icon name="user" type='FontAwesome' style={{color: '#384850'}}/>
                            <Input placeholder='Username' onChangeText={(_userName) => {
                                this.userCredentials.login.userName = _userName
                            }}/>
                        </InputGroup>
                    </Item>

                    <Item underline>
                        <InputGroup>
                            <Icon name="lock" type='FontAwesome' style={{color: '#384850'}}/>
                            <Input secureTextEntry blurOnSubmit placeholder='Password' onChangeText={(_password) => {
                                this.userCredentials.login.password = _password
                            }}/>
                        </InputGroup>
                    </Item>


                    <Button style={styles.button} block dark rounded
                            onPress={() => this.submit()}>
                        <Text style={styles.buttonText}>Login</Text>
                    </Button>
                </Container>
            </ScrollView>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        margin: 20
    },

    button: {
        flex: 1,
        justifyContent: 'center',
        margin: 5,
        color: '#fa3',
        marginTop: 15
    },
    buttonText: {
        color: '#fff',

    }
});

AppRegistry.registerComponent('LoginScreen', () => LoginScreen);


const mapStateToProps = (state: any) => {
    return {
        loginStatus: state.loginReducer.status,
        userCredentials: state.loginReducer.user
    }
};
const mapDispatchToProps = (dispatch: any) => {
    return {
        login: (credentials: object) => dispatch(loginWithCredentials(credentials))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);