import {Container, Content, Card, CardItem, Thumbnail, Text, Button} from 'native-base';
import React, {Component} from 'react';
import {StyleSheet, AppRegistry} from 'react-native';

/**
 * TODO: Update the style for that view later, it already has the data
 */
export default class Tweet extends Component<any, any> {

    render() {
        const {tweetData} = this.props;
        return (

            <Container style={styles.container}>

                <Content>
                    <Card>
                        <CardItem>
                            <Thumbnail source={{uri: tweetData.user.profile_image_url}}/>
                            <Text style={{padding: 9}}>{tweetData.user.name}</Text>
                            <Text style={{color: 'grey'}}>{'@' + tweetData.user.screen_name}</Text>
                        </CardItem>

                        <CardItem cardBody style={{padding: 10}}>
                            <Text>{tweetData.text}</Text>
                        </CardItem>
                        <CardItem footer>
                            <Text note>{tweetData.created_at}</Text>
                        </CardItem>
                    </Card>
                </Content>
            </Container>);
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: 'rgba(255,169,88,0)',
        paddingBottom: 0,
        height: 'auto',
        width: '100%'
    }
});

AppRegistry.registerComponent('Tweet', () => Tweet);
