import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    Button, AppRegistry, ScrollView, FlatList
} from 'react-native';
import {twitterApiFetch} from "../api/twitter/twitterSaga";
import {connect} from "react-redux";
import Tweet from "./twitter/tweetComponent";
import {STATUS} from "../api/requestStatus";
import {Container} from "native-base";

class HomeScreen extends Component<any, any> {
    loadTweets = () => {
        this.props.fetchTwitterApi();
    };

    componentDidMount(): void {
        this.loadTweets();
    }

    render() {
        let tweets;
        let status;
        if (this.props && this.props.twts && this.props.twts.status) {
            switch (this.props.twts.status) {
                case STATUS.FETCHING:
                    status = "Loading...";
                    break;
                case STATUS.FAILED:
                    status = 'Error';
                    break;
                case STATUS.SUCCEEDED:
                case STATUS.DONE:
                default:
                    status = null;
                    // tweets = this.props.twts.tweets.map((data: any) => {
                    //     return (<Tweet key={data.id} tweetData={data}/>);
                    // });
                    tweets = this.props.twts.tweets;
            }
        } else {
            status = null;
            tweets = null;
        }


        return <Container>
            { /**
            * TODO: Replace this button later with pull to refresh
            */}
            <Button title="Refresh Tweets" onPress={this.loadTweets}/>
            <Text style={{textAlign: 'center'}}>{status}</Text>
            <ScrollView>
                <FlatList
                    data={tweets}
                    keyExtractor={((item: any) => item.id.toString())}
                    renderItem={({item}) => <Tweet tweetData={item}/>}
                />
            </ScrollView>
        </Container>;
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
});


const mapStateToProps = (state: any) => {
    // ...state
    return {
        twts: state.twitterReducer
    }
};
const mapDispatchToProps = (dispatch: any) => {
    return {
        fetchTwitterApi: () => dispatch(twitterApiFetch())
    }
};


AppRegistry.registerComponent('HomeScreen', () => HomeScreen);
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);