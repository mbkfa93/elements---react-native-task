import {createDrawerNavigator, createStackNavigator, DrawerItems} from "react-navigation";

import LoginRegistrationScreen from '../components/login_registration';
import Home from "../components/Home";
import LoginScreen from '../components/login/login';
import RegistrationScreen from '../components/registration/registration';
import UserProfileScreen from '../components/userProfile/userProfile';


const AppNavigator = createDrawerNavigator({
        LoginRegistration: {
            screen: LoginRegistrationScreen,
            navigationOptions: {
                // drawerLockMode: 'locked-closed',
                drawerIcon: () => null,
                drawerLabel: () => null
            }
        },
        Login: {
            screen: LoginScreen,
            navigationOptions: {
                drawerLockMode: 'locked-closed',
                drawerIcon: () => null,
                drawerLabel: () => null
            }
        },
        Registration: {
            screen: RegistrationScreen,
            navigationOptions: {
                drawerLockMode: 'locked-closed',
                drawerIcon: () => null,
                drawerLabel: () => null
            }
        },
        Home: {
            name: "Home Screen",
            screen: Home,

        },
        UserProfile: {
            name: "User profile",
            screen: UserProfileScreen
        },
        logOut: {
            name: "Logout",
            screen: LoginRegistrationScreen,
        }

    },
    {
        // initialRouteName: "LoginRegistration"
    });

export default AppNavigator;
