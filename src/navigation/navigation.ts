import {createAppContainer} from "react-navigation";
import AppNavigator from "./screens";

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;